PYTHON = python3.6

FILMDIR ?= ./test/films

all: ${FILMDIR}/index.html ${FILMDIR}/index.de.html

award_categories.ttl: award_categories.py
	${PYTHON} $<

INFILES = ${FILMDIR}/.meta.ttl ${FILMDIR}/.contents.ttl
ANCILLARY = ~/git/omdb-semantics/movies.nt ~/git/omdb-semantics/trailers.nt ~/git/omdb-semantics/movie_links.nt ~/git/omdb-semantics/images.nt ~/git/omdb-semantics/movie-categories.nt
UNCONDITIONAL = award_categories.ttl ~/git/omdb-semantics/categories.nt
ANCILLARY_IGNORE = http://purl.org/dc/terms/subject

${FILMDIR}/index.ttl: ${INFILES} ${ANCILLARY} ${UNCONDITIONAL}
	${PYTHON} smush.py $@ \
		${FILMDIR}/.meta.ttl ${FILMDIR}/.contents.ttl \
		--ancillary ${ANCILLARY} \
		--unconditional ${UNCONDITIONAL} \
		--ancillary-ignore ${ANCILLARY_IGNORE} \
		--verbose --verbose

${FILMDIR}/.contents.ttl:
	${PYTHON} database.py --basedir ${FILMDIR}

${FILMDIR}/index.html: ${FILMDIR}/index.ttl
	${PYTHON} html.py --basedir ${FILMDIR}

${FILMDIR}/index.de.html: ${FILMDIR}/index.ttl
	# this is assuming that german speakers are prude
	${PYTHON} html.py --basedir ${FILMDIR} --outfile $@ --language de en --exclude-adult

.PHONY: all
