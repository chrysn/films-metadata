#!/usr/bin/env python3

"""Quickly written and not really performant RDF smusher. Replace this with
anything that can smush based on the configured identities and inverse
functional properties, and can output RDF with relative URIs to avoid seeing
file:/// in HTTP-served setups."""

from pathlib import Path
import logging
import argparse
import itertools
import functools

import rdflib

foaf = rdflib.Namespace('http://xmlns.com/foaf/0.1/')
owl = rdflib.Namespace('http://www.w3.org/2002/07/owl#')

inverse_functional = [
        foaf.isPrimaryTopicOf,
        ]
identities = [
        owl.sameAs,
        ]

def rank(u):
    u = str(u)
    if u.startswith('http://www.wikidata.org/entity/'):
        return (100, -len(u))
    elif u.startswith('http://dbpedia.org/'):
        return (90, -len(u))
    elif u.startswith('http://') and 'dbpedia.org/' in u and u.index('dbpedia.org') < u.index('/', 7):
        return (80, -len(u))
    elif u.startswith('https://'):
        return (11, -len(u))
    elif u.startswith('http://'):
        return (10, -len(u))
    return (0, -len(u))

def smush(graph):
    identity_groups = {}

    def identify(*args):
        if len(args) == 1:
            return
        newgroup = frozenset.union(frozenset(args), *(identity_groups.get(a, frozenset()) for a in args))

        for n in newgroup:
            identity_groups[n] = newgroup

    for p in identities:
        for s, o in graph.subject_objects(p):
            identify(s, o)

    for p in inverse_functional:
        for o in graph.objects(predicate=p):
            identify(*graph.subjects(p, o))

    nominal_names = {}

    for group in set(identity_groups.values()):
        ranked = sorted(group, key=rank, reverse=True)
        for r in ranked[1:]:
            nominal_names[r] = ranked[0]

    newgraph = rdflib.Graph()
    for s, p, o in graph:
        if p in identities:
            continue

        s = nominal_names.get(s, s)
        p = nominal_names.get(p, p)
        o = nominal_names.get(o, o)

        newgraph.add((s, p, o))

    for other, nominal in nominal_names.items():
        newgraph.add((nominal, identities[0], other))

    return newgraph

def run(outfile, infiles, ancillary=(), unconditional=(), ancillary_ignore=()):
    initial_graph = rdflib.Graph()

    for infile in infiles:
        logging.info("Loading %s...", infile)
        initial_graph.load(infile.__fspath__(), format="nt" if infile.suffix == ".nt" else "turtle")


    if ancillary:
        ancillary_graph = rdflib.Graph()

        for infile in ancillary:
            logging.info("Loading ancillary %s...", infile)
            ancillary_graph.load(infile.__fspath__(), format="nt" if infile.suffix == ".nt" else "turtle")

        logging.info("Pruning graph")
        processed = rdflib.Graph()
        pending = initial_graph
        unknown = ancillary_graph
        seen = set()
        logging.debug("Core graph contains %d statments, ancillary graphs %d.", len(pending), len(unknown))

        while pending:
            pending_terms = set()
            for stmt in pending:
                if stmt[1] in ancillary_ignore:
                    continue
                for term in stmt[::2]:
                    if term not in seen and not isinstance(term, rdflib.Literal):
                        pending_terms.add(term)
            logging.debug("Pruning run on %d new terms; %d statements can be moved to processed list. Still %d statements in unknown list.", len(pending_terms), len(pending), len(unknown))
            logging.debug("Some of the statements moved to processed now: %s", functools.reduce(lambda g, n: (g.add(n), g)[1], itertools.islice(pending, 3), rdflib.Graph()).serialize(format='ntriples').decode('utf8').strip())
            processed += pending
            pending = rdflib.Graph()
            # variant one -- kept around because it might be faster for differently shaped data
#            for term in pending_terms:
#                for foundstmt in itertools.chain(unknown.triples((term, None, None)), unknown.triples((None, None, term))):
#                    pending.add(foundstmt)
#                    unknown.remove(foundstmt)
            # variant two
            new_unknown = rdflib.Graph()
            for stmt in unknown:
                if any(t in pending_terms for t in stmt[::2]):
                    pending.add(stmt)
                else:
                    new_unknown.add(stmt)
            unknown = new_unknown
            # end variant two
            seen.update(pending_terms)

        graph = processed
        logging.debug("Resulting graph contains %d statements.", len(graph))
    else:
        graph = initial_graph

    for infile in unconditional:
        logging.info("Loading unconditional %s...", infile)
        graph.load(infile.__fspath__(), format="nt" if infile.suffix == ".nt" else "turtle")

    logging.info("Smushing URIs")

    oldlen = -1
    while oldlen != len(graph):
        oldlen = len(graph)
        graph = smush(graph)
        logging.debug("after smushing, length went from %d to %d", oldlen, len(graph)) # it's not the perfect metric as it only counts functional properties and not sameAs-es, but it's easy

    logging.info("Serializing result")

    if outfile.suffix == '.ttl':
        graph.serialize(outfile.as_posix(), format="turtle", base=rdflib.URIRef(outfile.parent.absolute().as_uri() + '/'))
    else:
        graph.serialize(outfile.as_posix(), format="nt")

def main():
    p = argparse.ArgumentParser()
    p.add_argument('outfile', help="Location to store the smushed data in", type=Path)
    p.add_argument('infiles', nargs='+', help="Files to read and smush", type=Path)
    p.add_argument('--ancillary', nargs='*', help="Files whose data will be added only when relevant to infiles", type=Path, metavar='FILE')
    p.add_argument('--unconditional', nargs='*', help="Files whose data will be added before smushing, but not considered for ancillary data relevance", type=Path, metavar='FILE')
    p.add_argument('--ancillary-ignore', nargs='+', help="Predicates to ignore when pruning (typical use would be dct:subject, and then load the SKOS file in unconditional)", type=rdflib.URIRef, metavar='URI')
    p.add_argument('--verbose', help="Show debug information (repeatable)", action='count')
    args = p.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG if args.verbose > 1 else logging.INFO)

    run(args.outfile, args.infiles, args.ancillary or (), args.unconditional or (), args.ancillary_ignore or ())

if __name__ == "__main__":
    main()
