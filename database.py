#!/usr/bin/env python3

import argparse
import sys
import re
import pickle
from hashlib import sha256
import urllib, urllib.parse
from pathlib import Path
import logging
import os
import warnings

import uritemplate
import rdflib


cachebase = Path(os.environ.get('XDG_CACHE_HOME', '~/.cache')).expanduser()

namepattern = re.compile('^(?P<title>.*) \((?P<year>[0-9]{4})( (?P<disambig>[^)]+))?\)$')

rdf = rdflib.Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
a = rdf.type
dbpedia2 = rdflib.Namespace('http://dbpedia.org/property/')
dbont = rdflib.Namespace('http://dbpedia.org/ontology/')
owl = rdflib.Namespace('http://www.w3.org/2002/07/owl#')
foaf = rdflib.Namespace('http://xmlns.com/foaf/0.1/')
dct = rdflib.Namespace('http://purl.org/dc/terms/')
skos = rdflib.Namespace('http://www.w3.org/2004/02/skos/core#')
rdfs = rdflib.Namespace('http://www.w3.org/2000/01/rdf-schema#')
ldp = rdflib.Namespace('http://www.w3.org/ns/ldp#')
frbr = rdflib.Namespace('http://purl.org/vocab/frbr/core#')
posix = rdflib.Namespace('http://www.w3.org/ns/posix/stat#')
schema = rdflib.Namespace('https://schema.org/')
hydra = rdflib.Namespace('http://www.w3.org/ns/hydra/core#')
void = rdflib.Namespace('http://rdfs.org/ns/void#')

ns = {k: v for (k,v) in locals().items() if isinstance(v, rdflib.Namespace)}

def path2uriref(p):
    return rdflib.URIRef(p.absolute().as_uri() + ("/" if p.is_dir() else ""))

def cached_load(url, **kwargs):
    """Memoizer on g = rdflib.Graph() / g.load keyed to the URL (kwargs are passed on but
    ignored for matching)"""

    pickleplace = cachebase / 'rdflib-load' / sha256(url.encode('utf8')).hexdigest()

    try:
        with pickleplace.open('rb') as pf:
            return pickle.load(pf)
    except (FileNotFoundError, EOFError):
        result = rdflib.Graph()
        try:
            result.load(url, **kwargs)
        except urllib.error.HTTPError as e:
            warnings.warn("HTTP error received (%s) from fragments set, reporting this as empty graph and not persisting it." % e)
            return rdflib.Graph()

        pickleplace.parent.mkdir(parents=True, exist_ok=True)
        with pickleplace.open('wb') as picklefile:
            pickle.dump(result, picklefile)

        return result

class Dataset:
    def __init__(self, url):
        g = cached_load(url, format='n3')
        searches = g.query("""SELECT ?template ?sname ?pname ?oname WHERE {
            ?x a hydra:Collection .
            ?x hydra:search ?s .
            ?s hydra:template ?template .
            ?s hydra:variableRepresentation hydra:ExplicitRepresentation .
            ?s hydra:mapping
                [ hydra:variable ?sname ; hydra:property rdf:subject ] .
            ?s hydra:mapping """ +
            # this repetition is necessary due to what's probably fixed in 4.2.2 along with https://github.com/RDFLib/rdflib/issues/381
            """
                [ hydra:variable ?pname ; hydra:property rdf:predicate ] .
            ?s hydra:mapping
                [ hydra:variable ?oname ; hydra:property rdf:object ] .
            }
        """, initNs=ns)
        (self.uri_template, *self.field_names), = (map(str, r) for r in searches)

    @staticmethod
    def _for_param(node):
        if node is None:
            return ''
        elif isinstance(node, rdflib.URIRef):
            return str(node)
        elif isinstance(node, rdflib.Literal):
            # that's not exactly turtle as pointed out in http://www.hydra-cg.com/spec/latest/core/
            return '"%s"'%node + ('@%s'%node.language if node.language else '') + ('^^%s'%node.datatype if node.datatype else '')
        else:
            raise NotImplementedError("Don't know how to represent %r in an ExplicitRepresentation query"%node)

    def query(self, s, p, o):
        filled = uritemplate.expand(self.uri_template, {k: self._for_param(v) for (k,v) in zip(self.field_names, (s, p, o))})

        actual_results = rdflib.Graph()

        next_uri = rdflib.URIRef(filled)
        while True:
            g = cached_load(next_uri, format='n3')
            for match in g.triples((s, p, o)):
                actual_results.add(match)

            next_uri_lines = list(g.objects(next_uri, hydra.next))
            if next_uri_lines:
                next_uri = next_uri_lines[0]
            else:
                break

        return actual_results

datasets = [Dataset(x) for x in ['http://fragments.dbpedia.org/en',]]

def get_cached(node):
    result = rdflib.Graph()

    for ds in datasets:
        result += ds.query(node, None, None)
        # that'd be the finer version
#        identity_data = ds.query(None, owl.sameAs, node)
#        identity_base = list(identity_data.subjects(owl.sameAs, node))
#        if identity_base:
#            current, = identity_base
#        else:
#            current = node
#
#        result += identity_data
#        result += ds.query(current, a, None)
    return result

def filename_to_uris(f):
    match = namepattern.match(f.name)
    if not match:
        raise ValueError

    data = match.groupdict()
    if data['disambig']:
        possible_names = [
                # required, for example, for "Aladdin (1992 Disney film)".
                # FIXME: come up with something clever when there happens to be
                # a film that does not exist in english wikipedia but needs a
                # disambiguation
                ('', '{title} ({year} {disambig})'.format(**data)),
                # required, for example, for "Nineteen Eighty-Four (TV
                # programme)" (as we can't trust redirects to not mean 303 See
                # Other)
                ('', '{title} ({disambig})'.format(**data)),
                ]
    else:
        possible_names = [
                ('', '{title} ({year} film)'.format(**data)),
                ('', '{title} ({year} film)'.format(**data)),
                ('', '{title} ({year})'.format(**data)), # eg. The Andromeda Strain (2008)
                ('de.', '{title} ({year})'.format(**data)),
                ('', data['title'] + ' (film)'),
                ('de.', data['title'] + ' (Film)'),
                ('', data['title']),
                ('de.', data['title']),
                ]
    # the apostrophe replacement seems to be an oddity of fragments.dbpedia.org
    # and was reported; the workaround is required as of 2017-10-17
    return [f"http://{prefix}dbpedia.org/resource/{n}".replace(' ', '_').replace('?', '%3F').replace("'", '') for (prefix, n) in possible_names]

class BaseWriter:
    def start(self):
        pass

    def odd_file(self, filename, reason):
        pass

    def indicate_rename(self, filename, source_url, dest_url, reason):
        # in general, no new file name can be constructed from dest_url -- one
        # of the reasons this only prints info and does not actually do any
        # renaming
        pass

    def record(self, filename, resource, metadata):
        pass

    def end(self):
        pass

class LogWriter(BaseWriter):
    def odd_file(self, filename, reason):
        print("File unknown: %s (%s)"%(filename, reason))

    def indicate_rename(self, filename, source_url, dest_url, reason):
        print("Rename suggested for %s (%s -> %s: %s)"%(filename, source_url, dest_url, reason))

    def record(self, f, node, results):
        printabletype = "is a film" if ((node, a, dbont.Film) in results) else  "is a TV show" if ((node, a, dbont.TelevisionShow) in results) else "NOT a film (but %s)" % " / ".join(str(x) for x in results.objects(node, a))
        print("Found film %s with metatada (%s, music by %s)"%(f, printabletype, [str(x) for x in results.objects(node, dbpedia2.music)]))
        for page in results.objects(node, foaf.isPrimaryTopicOf):
            print("    has a page on %s"%page)
        for imdbid in results.objects(node, dbont.imdbId):
            print("    has a imdb on http://www.imdb.com/title/tt%s/"%imdbid)
        try:
            name, *othernames = results.objects(node, foaf.name)
        except:
            print("    has no name!")
        else:
            if not f.name.startswith(name):
                print("    has different name than %s, probably already raised a rename warning"%name)
            if othernames:
                print("    has other names: %s"%(othernames,))
        #print(results.serialize(format="n3").decode())

class TTLWriter(BaseWriter):
    def __init__(self, filename):
        self._filename = filename
        self._container = path2uriref(filename.parent)

    def start(self):
        self.graph = rdflib.Graph()
        self.graph.add((self._container, a, ldp.Container))
        self.graph.add((self._container, a, ldp.BasicContainer))

        # required by linkeddata warp
        self.graph.add((self._container, a, posix.Directory))
        self.graph.add((self._container, posix.mtime, rdflib.Literal("0")))
        self.graph.add((self._container, posix.size, rdflib.Literal("0")))

    def odd_file(self, filename, reason):
        fileres = path2uriref(filename)
        self.graph.add((self._container, ldp.contains, fileres))
        self.graph.add((fileres, rdfs.comment, rdflib.Literal(reason)))

    def record(self, filename, resource, metadata):
        self.graph += metadata
        fileres = path2uriref(filename)
        self.graph.add((self._container, ldp.contains, fileres))

        # required by linkeddata warp
        self.graph.add((fileres, a, ldp.Container))
        self.graph.add((fileres, a, ldp.BasicContainer))
        self.graph.add((fileres, posix.mtime, rdflib.Literal("0")))
        self.graph.add((fileres, posix.size, rdflib.Literal("0")))

        the_supposed_embodiment = rdflib.BNode()
        self.graph.add((resource, frbr.embodiment, the_supposed_embodiment))
        self.graph.add((the_supposed_embodiment, frbr.exemplar, fileres)) # actually something *inside* fileres

    def end(self):
        logging.info("Serializing result")

        self.graph.serialize(self._filename.as_posix(), format="turtle")

def run(directory, filters, processors):
    allfiles = sorted(directory.iterdir())

    if filters:
        allfiles = [f for f in allfiles if any(arg.lower() in str(f).lower() for arg in filters)]

    ok_domains = {'http://dbpedia.org/', 'http://de.dbpedia.org'}

    for f in allfiles:
        if f.name.startswith('.'):
            continue

        if not f.is_dir():
            continue

        logging.info("Processing file %s", f)

        try:
            possible_uris = filename_to_uris(f)
        except ValueError:
            for p in processors:
                p.odd_file(f, "File name pattern not matched")
            continue

        for u in possible_uris:
            logging.info("Trying IRI %s", u)
            node = rdflib.URIRef(u)
            results = get_cached(node)
            types = list(results.objects(node, a))
            if not types:
                continue

            logging.info("Found %d statements", len(results))

            # r != node: the 2016-04 fragments export contained some identity redirect statements
            redirects = [r for r in results.objects(node, dbont.wikiPageRedirects) if r != node and any(r.startswith(o) for o in ok_domains)]
            if redirects:
                target, = redirects
                if str(target) in possible_uris:
                    continue
                else:
                    for p in processors:
                        p.indicate_rename(f, node, target, "redirect")

                    # it seems to be better to stick with the redirect.
                    # resolving redirects is sometimes beneficial, but those
                    # are few cases, and they can often be resolved by
                    # following redirect hints. on the other hand, following
                    # redirects can break semantics (eg. de:Tierische_Liebe ->
                    # de:Ulrich_Seidl).
                    #node = target
                    #results = results + get_cached(target)

            identities = {r for r in results.objects(node, owl.sameAs) if any(r.startswith(o) for o in ok_domains) and r != node}
            for i in identities:
                identity_results = get_cached(i)
                logging.info("Found %d statements from identity %s", len(identity_results), i)
                results = results + identity_results
            for target in [r for r in results.objects(node, owl.sameAs) if r.startswith('http://dbpedia.org/') and r != node and r not in possible_uris]: # more of an if
                for p in processors:
                    p.indicate_rename(f, node, target, "Identity found from localized name")

            for p in processors:
                p.record(f, node, results)

            break
        else:
            for p in processors:
                p.odd_file(f, "Not found at all")

def main():

    p = argparse.ArgumentParser()
    p.add_argument('filter', nargs='*', help="Limit this run to files containing one of the given search terms")
    p.add_argument('--no-ttl', help="Do not produce an .contents.ttl file (only go through name heuristics)", action='store_false', dest='ttl', default=True)
    p.add_argument('--verbose', help="Show debug information", action='count')
    p.add_argument('--basedir', help="Directory that contains films, and in which the .contents.ttl file is created", default=".", type=Path, metavar="DIR")
    args = p.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG if args.verbose > 1 else logging.INFO)

    processors = [LogWriter()]

    if args.ttl:
        ttlwriter = TTLWriter(args.basedir / '.contents.ttl')
        processors.append(ttlwriter)

    for p in processors:
        p.start()

    run(args.basedir, args.filter, processors)

    for p in processors:
        p.end()

if __name__ == "__main__":
    main()
