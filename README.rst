This project provies tools for finding metadata from movie folders via
DBpedia, merging in other data sources, and building an HTML index for it.

Quick start
===========

To generate a sample page based on the almost empty demo film list at
./test/films/, simply run `make` in this directory.

Parts
=====

It consists of discrete components:

* database.py looks up DBpedia articles from file names using fixed rules.
  Directory names need to be 'Film title (year)', and DBpedia articles are
  assumed to be named 'Film title (year film)', 'Film title (year)' or 'Film
  title'; German DBpedia is also queried for 'Film title (Film)' and similar.

  This should further be split up into a file that contains the statements
  about the directories and another location that contains local copies of the
  dbpedia exports, but those need to run in lockstep because matching depends
  on whether the dbpedia page exists, and in future maybe even on its
  contents.

* award_categories.py looks up generic metadata about awards from DBpedia
  using a SPARQL query.

* Data from OMDB is fetched independently by the tool at
  https://gitlab.com/chrysn/omdb-semantics

* All that data is run through smush.py, which loads all RDF data, smushes
  them, and prunes the tree; "pruning" here is used to describe a process
  where all statements are removed that can't realistically be reached from
  a set of start resources. (Realistically here means not searching for
  statements by their predicates, and excluding some statements because they
  could be used to match anything).

* html.py takes that data and makes a nice index.html out of it.

* All this is coordinated by a Makefile. The one shipped can be used as-is,
  but probably needs tuning to match the users' language preferences.

State of the project
====================

This is right now primarily a toy project for me to experiment with RDF
techniques (getting to know SPARQL, trying out graph pruning, comparing static
HTML generation from RDF to loading RDF into Javascript and working from
there).

Unless your movie collection is organized like mine, chances are slim it will
do anything useful for you. If you adapt any part of it for whichever use
case, I'd be curious to hear about it!

Further ideas
=============

* Generate data for greasemonkey scripts: Have Wikipedia articles display a
  "I have a local copy of this film, play it now".

* Create gvfs metadata like icons (as used by nautilus/nemo, queriedusing
  gvfs-info metadata::custom-icon and stored in binary at
  ~/.local/share/gvfs-metadata).

* Drill deeper into the directories to see which language versions are
  available. (This is quite easy with everything that contains a single file
  with good in-format metadata, becomes unreliable when the file's language
  has to be guessed from the file name, becomes tricky when people start
  having films available in the old and the new localization, and if that was
  solved, people who have different cuts of films available would want that to
  be considered too).

* Explore how this can be generalized into "file system linked data" (a bit
  like JSON-LD but for directory structures)

------

Use, copy, modify, share -- terms of the MIT license apply.
