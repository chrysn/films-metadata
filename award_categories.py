#!/usr/bin/env python3

import rdflib
from pathlib import Path
import os
from hashlib import sha256
import pickle
import time

from SPARQLWrapper import SPARQLWrapper, RDF, POST
from SPARQLWrapper.Wrapper import QueryResult

# monkey patch for broken code
#
# original raises `AttributeError: 'Request' object has no attribute 'add_data'` in SPARQLWrapper 1.6.0
import urllib

def _createRequest(self):
    """Internal method to create request according a HTTP method. Returns a
    C{urllib2.Request} object of the urllib2 Python library
    @return: request
    """
    if self.isSparqlUpdateRequest():
        uri = self.updateEndpoint
    else:
        uri = self.endpoint

    encodedParameters = urllib.parse.urlencode(self._getRequestParameters(), True)

    if self.method == POST:
        request = urllib.request.Request(uri, data=encodedParameters.encode('ascii'))
        request.add_header("Content-Type", "application/x-www-form-urlencoded")
    else:  # GET
        request = urllib.request.Request(uri + "?" + encodedParameters)

    request.add_header("User-Agent", self.agent)
    request.add_header("Accept", self._getAcceptHeader())
    if self.user and self.passwd:
        request.add_header("Authorization", "Basic " + base64.encodestring("%s:%s" % (self.user, self.passwd)))

    return request
SPARQLWrapper._createRequest = _createRequest

# original uses ' ' for publicID, which raises "does not look like a valid URI,
# trying to serialize this will break." warnings.
def _convertRDF(self):
    """
    Convert a RDF/XML result into an RDFLib triple store. This method can be overwritten
    in a subclass for a different conversion method.
    @return: converted result
    @rtype: RDFLib Graph
    """
    try:
        from rdflib.graph import ConjunctiveGraph
    except ImportError:
        from rdflib import ConjunctiveGraph
    retval = ConjunctiveGraph()
    # this is a strange hack. If the publicID is not set, rdflib (or the underlying xml parser) makes a funny
    #(and, as far as I could see, meaningless) error message...
    retval.load(self.response, publicID='sparqlwrapperhack')
    return retval
QueryResult._convertRDF = _convertRDF


cachebase = Path(os.environ.get('XDG_CACHE_HOME', '~/.cache')).expanduser()

ep = SPARQLWrapper("http://dbpedia.org/sparql")
ep.setMethod(POST)
ep.setReturnFormat(RDF)

def query_cached(q, ep=ep):
    encoded = sha256(ep.endpoint.encode('utf8') + q.encode('utf8')).hexdigest()

    p = cachebase / 'dbpedia-sparql' / encoded

    try:
        return pickle.load(p.open('rb'))
    except (FileNotFoundError, EOFError):
        ep.setQuery(q)
        plain = ep.query()
        result = plain.convert()
        p.parent.mkdir(parents=True, exist_ok=True)
        pickle.dump(result, p.open('wb'))

        time.sleep(1)
        return result

def get_generic_data():
    # flattening this regardless of transitivity makes later use so much easier
    broader_query = "CONSTRUCT {?aw skos:broader ?res. ?aw rdfs:label ?label .} WHERE {?aw skos:broader+ ?res . ?aw rdfs:label ?label . }"
    oscar = rdflib.URIRef('http://dbpedia.org/resource/Category:Academy_Award_winners')
    other = rdflib.URIRef('http://dbpedia.org/resource/Category:Film_award_winners')

    q = broader_query.replace("?res", oscar.n3())
    results = query_cached(q)
    q = broader_query.replace("?res", other.n3())
    results += query_cached(q)

    return results

def main():
    get_generic_data().serialize('award_categories.ttl', format='turtle')

if __name__ == "__main__":
    main()
