#!/usr/bin/env python3

import pickle
from pathlib import Path
import urllib
import argparse
import json
import logging
import os
import base64
from itertools import chain
import datetime

from markupsafe import Markup
import rdflib

cachebase = Path(os.environ.get('XDG_CACHE_HOME', '~/.cache')).expanduser()

language_preference = ('en', None)

rdf = rdflib.Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
a = rdf.type
dbont = rdflib.Namespace('http://dbpedia.org/ontology/')
owl = rdflib.Namespace('http://www.w3.org/2002/07/owl#')
foaf = rdflib.Namespace('http://xmlns.com/foaf/0.1/')
dct = rdflib.Namespace('http://purl.org/dc/terms/')
skos = rdflib.Namespace('http://www.w3.org/2004/02/skos/core#')
rdfs = rdflib.Namespace('http://www.w3.org/2000/01/rdf-schema#')
ldp = rdflib.Namespace('http://www.w3.org/ns/ldp#')
frbr = rdflib.Namespace('http://purl.org/vocab/frbr/core#')
posix = rdflib.Namespace('http://www.w3.org/ns/posix/stat#')
schema = rdflib.Namespace('https://schema.org/')
exif = rdflib.Namespace('https://www.w3.org/2003/12/exif/ns#')
ns = {k: v for (k,v) in locals().items() if isinstance(v, rdflib.Namespace)}

def load(bigttl):
    cachefile = bigttl.with_suffix('.ttl.pkl')
    if cachefile.exists() and cachefile.stat().st_mtime > bigttl.stat().st_mtime:
        return pickle.load(cachefile.open('rb'))

    graph = rdflib.Graph()
    container = graph.absolutize(rdflib.URIRef(bigttl.__fspath__()))
    graph.load(container, format="n3")

    if bigttl.name == 'index.ttl':
        # For concistency with (currently hypothetical) users from a browser
        # context that load it as '/' rather than '/index.ttl'
        container = graph.absolutize(rdflib.URIRef(bigttl.parent.__fspath__() + '/'))

    with cachefile.open('wb') as cachehandle:
        pickle.dump((container, graph), cachehandle, pickle.HIGHEST_PROTOCOL)

    return container, graph

_label_and_alternatives_guess = object()
def label_and_alternatives(graph, res, default=_label_and_alternatives_guess):
    candidates = []
    for lang in language_preference:
        candidates.extend(value for (prop, value) in graph.preferredLabel(res, labelProperties=(skos.prefLabel, foaf.name, rdfs.label), lang=lang))
    candidates = [c for c in candidates if not c.startswith('(')] # workaround for garbage in wikipedia articles
    if not candidates:
        if default == _label_and_alternatives_guess:
            return (str(res), )
        else:
            return (default, )
    seen = set()
    candidates = [c for c in candidates if (str(c) not in seen, seen.add(str(c)))[0]]
    return candidates

def label(graph, res):
    """Give the best available label for res considering language preference"""

    # this could be more performant, but this is easiest
    return label_and_alternatives(graph, res)[0]

def links_for(graph, expression):
    """For a resource 'expression', give a list of HTML links that are on topic"""
    homepage_urls = graph.objects(expression, foaf.homepage)
    links = [Markup('<a href="%s">⌂</a>')%u for u in homepage_urls]
    trailer_urls = graph.objects(expression, schema.trailer)
    links += [Markup('<a href="%s">Trailer</a>')%u for u in trailer_urls]
    topic_urls = set(list(graph.objects(expression, foaf.isPrimaryTopicOf)) + ['http://www.imdb.com/title/tt%s/'%i for i in graph.objects(expression, dbont.imdbId)])
    links += [
            Markup('<a href="%s">%s</a>')%(x,
                "WP:%s"%(x[7:x.find('.wikipedia.org')]) if '.wikipedia.org' in x else
                "IMDB" if "//www.imdb.com/" in x else
                "OMDB" if "//www.omdb.org/" in x else
                "external"
                )
            for x in sorted(topic_urls)]
    expression_urls = set([expression] + list(graph.objects(expression, owl.sameAs)))
    # filtering out non-dereferencables and dbpedia localizations, which make too much noise
    links += [Markup('<a href="%s">DB</a>') % u
            for u
            in sorted(expression_urls)
            if isinstance(u, rdflib.URIRef)
                and "dbpedia.org" not in u
                and "rdf.freebase.com" not in u
                and "www.viaf.org" not in u
                and "www.wikidata.org" not in u
                and not u.startswith('tag:')
                and not u.startswith('file:')
            ]

    return links

def file_to_data_uri(p):
    data = p.open('rb').read()
    # using application/octet-stream avoids guessing on our side; i'd rather
    # have the browser blame issues with odd file formats on their own guessing
    # than on ours.
    return 'data:application/octet-stream;base64,' + base64.encodebytes(data).decode('ascii')

def filename_filter(keywords):
    """Create a filter function that matches iff any of the given keywords is in the file name"""
    def _filter(graph, fileres):
        return any(f.lower() in str(fileres).lower() for f in keywords)
    return _filter

def exclude_adult(graph, fileres):
    """Filter function that returns False iff the film is tagged 'Adult' in OMDB"""
    return not graph.query('ASK { ?e frbr:embodiment ?m . ?m frbr:exemplar ?r . ?e dct:subject ?adultcat . ?adultroot skos:narrower* ?adultcat . ?adultroot foaf:isPrimaryTopicOf <https://www.omdb.org/encyclopedia/category/867> . }', initNs=ns, initBindings=dict(r=fileres)).__iter__().__next__()

def filter_all(filters):
    """Filter function that works like a deferred map(all, ...), defaulting to True on an empty filters list"""
    def _filter(graph, fileres):
        if not filters:
            return True
        return all(f(graph, fileres) for f in filters)
    return _filter

def write_html(filename, bigttl, *, filters=lambda *args: True, cache_images=True):
    container, graph = load(bigttl)

    if (container, a, ldp.BasicContainer) not in graph:
        logging.info("Assumed container is %s; shortest-named available container is %s", container, min(graph.subjects(a, ldp.BasicContainer), key=lambda x: len(str(x))))
        raise Exception("Container is not a BasicContainer, aborting to avoid creating erroneous output")

    refbase = filename.parent.absolute().as_uri() + '/'

    imgwidth = "5em"
    css = Markup("""
        body { background-color:silver; color:black; font-family:sans; text-align:center; overflow-wrap:break-word; }
        ul { padding:0; margin:0; max-width: 60em; display:inline-block; text-align:left; }
        li.filmseries ul { max-width: auto; width: 100%; }
        ul li { display:block; padding:0.5em; margin:0.5em; background-color:white; border:thin solid gray; border-radius:0.5em; }
        ul li.filmseries { background-color: whitesmoke; }
        .imglink { float:left; margin-right:1em; }
        .imglink img { width:""" + imgwidth + """; height: auto; }
        .title { display:block; font-size:150%; margin-bottom:0.5em; }
        .awards { display:block; float:right; }
        .othernames { display:block; }
        .links { display:block; }
        .links:after { content:""; clear:both; display:block; height:0; visibility:hidden; }

        @media (max-width: 60em) {
                ul { max-width: 100%; }
        }
        @media (max-width: 30em) {
                body { margin: 0; }
                ul li { margin:0.5em 0; }
        }
        """) # needs to be markup for otherwise '"' gets html-escaped, and browsers don't like that

    html_full = Markup(
            '<!doctype html>'
            '<html><head>'
            '<meta charset="utf-8" />'
            '<meta name="viewport" content="width=device-width, initial-scale=1" />'
            '<title>Index of films</title>'
            '<style>{css}</style>'
            '</head><body>'
            '<ul>{items}</ul>'
            '</body></html>')

    items = []
    groupitems = {}

    oscar = rdflib.URIRef('http://dbpedia.org/resource/Category:Academy_Award_winners')
    any_award = rdflib.URIRef('http://dbpedia.org/resource/Category:Film_award_winners')
    awards_labels = {x: (label(graph, x), '★' if (x, skos.broader, oscar) in graph else '☆') for x in graph.subjects(skos.broader, any_award)}

    for fileres in graph.objects(container, ldp.contains):
        if not filters(graph, fileres):
            continue

        expressions = [m for (m,) in graph.query("SELECT DISTINCT ?e WHERE { ?e frbr:embodiment ?m . ?m frbr:exemplar ?r . }", initNs=ns, initBindings=dict(r=fileres))]

        assert fileres.startswith(refbase), "fileres %s not inside rebase %s"%(fileres, refbase)
        # the ./ is required for all cases that contain a colon before a slash
        filelink = "./" + fileres[len(refbase):]

        fallback_name = [x for x in urllib.parse.unquote(fileres).split('/') if x][-1]

        if not expressions:
            title = fallback_name
            items.append((title, Markup('<li><span class="title"><a href="{file}">{title}</a></span> <span class="oddfilereason">{reason}</span></li>').format(file=filelink, reason=" / ".join(graph.objects(fileres, rdfs.comment)), title=title), None))
            continue

        expression, = expressions

        names = label_and_alternatives(graph, expression, fallback_name)

        imdbs = sorted(list(graph.objects(expression, dbont.imdbId)) + [u[28:].strip('/') for u in graph.objects(expression, foaf.isPrimaryTopicOf) if u.startswith('http://www.imdb.com/title/tt')])

        if cache_images:
            image_opener = urllib.request.URLopener()
            def maybe_cached(image):
                cachepath = filename.parent / '.image-cache' / image.split('://', 1)[1]
                if not cachepath.exists():
                    cachepath.parent.mkdir(exist_ok=True, parents=True)
                    image_opener.retrieve(image, filename=cachepath.__fspath__())
                return cachepath.relative_to(str(filename.parent) + '/')
        else:
            maybe_cached = lambda x: x

        depictions = sorted(graph.subjects(foaf.depicts, expression))
        if depictions:
            image = depictions[0]
            nosrcset_width = None
            versions = {}
            for i in chain((image,), graph.objects(image, foaf.thumbnail)):
                widths = [int(x) for x in graph.objects(i, exif.imageWidth)]
                if not widths:
                    continue
                width, = widths
                if nosrcset_width is None or width < nosrcset_width:
                    nosrcset_width = width
                    nosrcset_image = i
                versions[width] = i
            if versions:
                srcset = Markup(' srcset="%s"')%(", ".join("%s %dw"%(maybe_cached(v), k) for (k, v) in versions.items()))
            else:
                srcset = ""
            # Setting width and height to give the browser an aspect ratio to
            # work with while flowing content not yet loaded. Giving the
            # correct aspect ratio for this would be better, but we don't have
            # the information at hand easily (it's not in the OMDB export, and
            # as long as caching is conditional, this would all be very
            # complicated).
            #
            # The estimate will only be used until the (an) image is loaded,
            # then the CSS height:auto will take over.
            #
            # Should be good enough to make things not *too* jumpy during lazy
            # loading.
            posterfragment = Markup('<img src="%s" alt="Poster"%s sizes="%s" width="100" height="150" loading="lazy" />')%(maybe_cached(nosrcset_image), srcset, imgwidth)
        else:
            posterfragment = ""


        awards = {cat: labels for (cat, labels) in awards_labels.items() if (expression, dct.subject, cat) in graph}
        awards = Markup("").join(Markup(' <a href="%s" title="%s">%s</a> ')%(cat, text, icon) for (cat, (text, icon)) in sorted(awards.items()))

        links = links_for(graph, expression)

        genre = Markup(" / ").join(label(graph, g) for (g,) in graph.query("SELECT ?g WHERE { ?e dct:subject ?g . ?g skos:inScheme [ foaf:isPrimaryTopicOf <https://www.omdb.org/encyclopedia/category/1> ]. }", initNs=ns, initBindings={'e': expression}))

        if len(names) > 1:
            othernames = Markup(" / ").join(names[1:])
        else:
            othernames = ""

        sortname = str(names[0])
        body = Markup('<li><a href="{file}" class="imglink">{poster}</a><span class="title"><a href="{file}">{name}</a> <span class="awards">{awards}</span></span><span class="othernames">{othernames}</span><span class="genre">{genre}</span><span class="links">{links}</span></li>').format(name=names[0], othernames=othernames, file=filelink, links=Markup(", ").join(links), poster=posterfragment, awards=awards, genre=genre)

        parents = list(graph.subjects(frbr.part, expression))
        for parent in parents: # more of an if
            if parent not in groupitems:
                groupitems[parent] = []
            groupitems[parent].append((sortname, body, expression))
        if not parents:
            items.append((sortname, body, expression))

    for (k, v) in groupitems.items():
        if len(v) == 1:
            items.append(v[0])
        else:
            grouplabel = label(graph, k)
            grouplinks = Markup(", ").join(links_for(graph, k))
            items.append((
                str(grouplabel),
                Markup('<li class="filmseries"><h1>{grouplabel}</h1>{grouplinks}<ul>{items}</ul></li>')
                    .format(
                        grouplabel=grouplabel,
                        grouplinks=grouplinks,
                        items=Markup("\n").join(
                            body
                            for (sorttitle, body, expression)
                            # ', r[0])': stable fall-back if no date is present
                            in sorted(v, key=lambda r: (
                                max(graph.objects(r[2], schema.datePublished), default=datetime.date(1, 1, 1)),
                                r[0]))
                            )
                ),
                k,
            ))

    with filename.open("w") as outfile:
        outfile.write(html_full.format(css=css, items=Markup("\n").join(body for (sorttitle, body, _) in sorted(items))))

def main():
    p = argparse.ArgumentParser()
    p.add_argument('--basedir', help="Directory that contains films, and in which the index.* files are used/created", default=".", type=Path, nargs='?')
    p.add_argument('--verbose', help="Show debug information", action='count')
    p.add_argument('--language', help="Language to prefer in labels (multiple values indicate descending preference, default: %(default)s", nargs='+', default=('en',), metavar='LANG')
    p.add_argument('--outfile', help="Output file name (relative to basedir or absolute, default: %(default)s)", default='index.html', metavar='FILE')
    p.add_argument('--exclude-adult', help="Exclude all items that are in OMDB's adult section", action='store_true')
    p.add_argument('filter', nargs='*', help="Limit this run to files containing one of the given search terms")
    args = p.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG if args.verbose > 1 else logging.INFO)

    global language_preference
    language_preference = tuple(args.language) + ('', )

    filters = []
    if args.filter:
        filters.append(filename_filter(args.filter))

    if args.exclude_adult:
        filters.append(exclude_adult)

    write_html(args.basedir / args.outfile, args.basedir / 'index.ttl', filters=filter_all(filters))

if __name__ == "__main__":
    main()
